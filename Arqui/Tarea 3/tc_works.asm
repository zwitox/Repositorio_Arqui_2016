%include  "io.mac"

.DATA
date_msg      db   'Please enter the desired date in dd format: ',0
month_msg      db   'Please enter the desired month in mm format: ',0
year_msg      db   'Please enter the desired year in yyyy format: ',0
confirm_msg1  db   '===>',0
confirm_msg2  db   ' resultado final ==>',0 
welcome_msg   db   'month is less than 3',0
leap_year     db   ' is a leap year',0
not_leap_year db   ' is not a leap year',0	  
calendar_system dd 1 ;  1 will return results based on the Gregorian Calendar and 0 will return results based on the Julian Calendar
;Letters
linebreak db 10,0	  
S1 db 'SSSS ',0
S2 db 'S    ',0
S4 db '   S ',0
A1 db ' AA  ',0
A2 db 'A  A ',0
A3 db 'AAAA ',0
B1 db 'BBB  ',0
B2 db 'B  B ',0
B3 db 'BBBB ',0
D1 db 'DDD  ',0
D2 db 'D  D ',0
O1 db ' OO  ',0
O2 db '0  0 ',0
M1 db 'M   M ',0
M3 db 'MM MM ',0
I1 db 'IIII ',0
I2 db ' II  ',0
N1 db 'N  N ',0
N3 db 'NNNN ',0
N4 db 'N NN ',0
G1 db 'GGGG ',0
G2 db 'G    ',0
G3 db 'G GG ',0
G4 db 'G  G ',0
L1 db 'L    ',0
L5 db 'LLLL ',0
U1 db 'U  U ',0
U5 db ' UU  ',0
E1 db 'EEEE ',0
E2 db 'E    ',0
R1 db 'RR   ',0
R2 db 'R RR ',0
R4 db 'R R  ',0
R5 db 'R  R ',0
T1 db 'TTTTT ',0
T2 db '  T  ',0
C1 db 'CCCC ',0
C2 db 'C    ',0
J1 db '   J ',0
J4 db 'J  J ',0
J5 db ' JJ  ',0
V1 db 'V   V ',0
V4 db ' V V  ',0
V5 db '  V   ',0
Y1 db 'Y   Y',0
Y2 db ' Y Y',0
Y3 db '  Y  ',0
W1 db 'W   W',0
W3 db 'W W W',0
W4 db 'WW WW',0
H1 db 'H   H',0
H3 db 'HHHHH',0
F1 db 'FFFFF',0
F2 db 'F    ',0
F3 db 'FFF  ',0
;End letters
.UDATA
user_name    resb  16             ; buffer for user name
user_date    resb  9
response     resb  1
day 	     resd  1
month 	     resd  1
year         resd  1
temp 		 resb  1
leap 		 resb  1


.CODE
     .STARTUP
     PutStr  date_msg         ; prompt user for date
     GetLInt [day]
     PutStr  month_msg
     GetLInt [month]
     PutStr  year_msg
     GetLInt [year]
     xor EAX,EAX
     mov eax,[month]
     cmp eax,3
     jg minus_3
     mov EAX,12
     add [month],EAX
     mov EAX,1
     sub [year],EAX
     minus_3: 
     mov EAX,[month]
     mov EBX,2
     mul EBX
     add EAX,[day]
     mov [temp],EAX
     ;PASO 1 (day+(2*month)) //stores the result in temp
     xor EAX,EAX
     mov EAX,[month]
     add EAX,1
     mov EBX,6
     mul EBX
     mov EBX,10
     div EBX
     add [temp],EAX
     ;PASO 2 (temp+(6*(month+1)/16)) stores result in temp
     mov EAX,[year]
     add [temp],EAX
     ;PASO 3 (temp + year) stores result in temp
     mov EAX,[year]
     mov EBX,4
     xor EDX,EDX
     div EBX
     add [temp],EAX
     ;PASO 4 (temp+(year/4)) stores result in temp
	 mov EAX,[year]
     mov EBX,100
     xor EDX,EDX
     div EBX
     sub [temp],EAX
     ;PASO 5 (temp-(year/100)) stores result in temp
     mov EAX,[year]
     mov EBX,400
     xor EDX,EDX
     div EBX
     add [temp],EAX
     ;PASO 6 (temp+(year/400)) stores result in temp
     mov EAX,[calendar_system]
     add [temp],EAX
     ;PASO 7 (temp+calendar_system)
     xor EAX,EAX
     mov EAX,[temp]
     mov EBX,7
     xor EDX,EDX
     div EBX
     mov [temp],EDX
     ;PASO 8 (temp%7)
     xor EAX,EAX
  	 PutStr linebreak
  	 mov EAX,[temp]
  	 
  	 cmp EAX,0
  	 jne a
  	 call Domingo
	 PutStr linebreak
	 call Sunday
  	 a:
  	 cmp EAX,1
  	 jne b
  	 call Lunes
	 PutStr linebreak
	 call Monday
  	 b:
  	 cmp EAX,2
  	 jne c
  	 call Martes
	 PutStr linebreak
 	 call Tuesday
  	 c:
  	 cmp EAX,3
  	 jne d
  	 call Miercoles
 	 PutStr linebreak
 	 call Wednesday
  	 d:
  	 cmp EAX,4
  	 jne e
  	 call Jueves
	 PutStr linebreak
 	 call Thursday
  	 e:
  	 cmp EAX,5
  	 jne f
  	 call Viernes
	 PutStr linebreak
	 call Friday
  	 f:
  	 cmp EAX,6
  	 jne g
  	 call Sabado
	 PutStr linebreak
	 call Saturday
  	 g:
  	 ;Leap year
  	 ;Test if year %4
  	 xor EAX,EAX
  	 mov EAX,[year]
  	 mov EBX,4
  	 xor EDX,EDX
  	 div EBX
  	 cmp EDX,0
  	 je div_4
  	 ;Test if year%100==0
  	 xor EAX,EAX
  	 mov EAX,[year]
  	 mov EBX,4
  	 xor EDX,EDX
  	 div EBX
  	 cmp EDX,0
  	 je div_4
  	 PutStr linebreak
  	 PutInt[year]
  	 PutStr not_leap_year
  	 PutStr linebreak
  	 jmp end
  	 div_4:
  	 xor EAX,EAX
  	 mov EAX,[year]
  	 mov EBX,400
  	 xor EDX,EDX
  	 div EBX
  	 cmp EDX,0
  	 PutStr linebreak
  	 PutInt[year]
  	 PutStr leap_year
  	 PutStr linebreak
  	 jmp end
  	 ;Init letter print
  	 Lunes:
	 PutStr L1
	 PutStr U1
	 PutStr N1
	 PutStr E1
	 PutStr S1
	 PutStr linebreak
	 PutStr L1
	 PutStr U1
	 PutStr N1
	 PutStr E2
	 PutStr S2
	 PutStr linebreak
	 PutStr L1
	 PutStr U1
	 PutStr N3
	 PutStr E1
	 PutStr S1
	 PutStr linebreak
	 PutStr L1
	 PutStr U1
	 PutStr N4
	 PutStr E2
	 PutStr S4
	 PutStr linebreak
	 PutStr L5
	 PutStr U5
	 PutStr N1
	 PutStr E1
	 PutStr S1
	 PutStr linebreak
	 ret
	 
	 Martes:
	 PutStr M1
	 PutStr A1
	 PutStr R1
	 PutStr T1
	 PutStr E1
	 PutStr S1
	 PutStr linebreak
	 PutStr M1
	 PutStr A2
	 PutStr R2
	 PutStr T2
	 PutStr E2
	 PutStr S2
	 PutStr linebreak
	 PutStr M3
	 PutStr A3
	 PutStr R1
	 PutStr T2
	 PutStr E1
	 PutStr S1
	 PutStr linebreak
	 PutStr M1
	 PutStr A2
	 PutStr R4
	 PutStr T2
	 PutStr E2
	 PutStr S4
	 PutStr linebreak
	 PutStr M1
	 PutStr A2
	 PutStr R5
	 PutStr T2
	 PutStr E1
	 PutStr S1
	 PutStr linebreak
	 ret 
	 
	 Miercoles:
	 PutStr M1
	 PutStr I1
	 PutStr E1
	 PutStr R1
	 PutStr C1
	 PutStr O1
	 PutStr L1
	 PutStr E1
	 PutStr S1
	 PutStr linebreak
	 PutStr M1
	 PutStr I2
	 PutStr E2
	 PutStr R2
	 PutStr C2
	 PutStr O2
	 PutStr L1
	 PutStr E2
	 PutStr S2
	 PutStr linebreak
	 PutStr M3
	 PutStr I2
	 PutStr E1
	 PutStr R1
	 PutStr C2
	 PutStr O2
	 PutStr L1
	 PutStr E1
	 PutStr S1
	 PutStr linebreak
	 PutStr M1
	 PutStr I2
	 PutStr E2
	 PutStr R4
	 PutStr C2
	 PutStr O2
	 PutStr L1
	 PutStr E2
	 PutStr S4
	 PutStr linebreak
	 PutStr M1
	 PutStr I1
	 PutStr E1
	 PutStr R5
	 PutStr C1
	 PutStr O1
	 PutStr L5
	 PutStr E1
	 PutStr S1
	 PutStr linebreak
	 ret 
	 
	 Jueves:
	 PutStr J1
	 PutStr U1
	 PutStr E1
	 PutStr V1
	 PutStr E1
	 PutStr S1
	 PutStr linebreak
	 PutStr J1
	 PutStr U1
	 PutStr E2
	 PutStr V1
	 PutStr E2
	 PutStr S2
	 PutStr linebreak
	 PutStr J1
	 PutStr U1
	 PutStr E1
	 PutStr V1
	 PutStr E1
	 PutStr S1
	 PutStr linebreak
	 PutStr J4
	 PutStr U1
	 PutStr E2
	 PutStr V4
	 PutStr E2
	 PutStr S4
	 PutStr linebreak
	 PutStr J5
	 PutStr U5
	 PutStr E1
	 PutStr V5
	 PutStr E1
	 PutStr S1
	 PutStr linebreak
	 ret
	 Viernes:
	 PutStr V1
	 PutStr I1
	 PutStr E1
	 PutStr R1
	 PutStr N1
	 PutStr E1
	 PutStr S1
	 PutStr linebreak
	 PutStr V1
	 PutStr I2
	 PutStr E2
	 PutStr R2
	 PutStr N1
	 PutStr E2
	 PutStr S2
	 PutStr linebreak
	 PutStr V1
	 PutStr I2
	 PutStr E1
	 PutStr R1
	 PutStr N3
	 PutStr E1
	 PutStr S1
	 PutStr linebreak
	 PutStr V4
	 PutStr I2
	 PutStr E2
	 PutStr R4
	 PutStr N4
	 PutStr E2
	 PutStr S4
	 PutStr linebreak
	 PutStr V5
	 PutStr I1
	 PutStr E1
	 PutStr R5
	 PutStr N1
	 PutStr E1
	 PutStr S1
	 PutStr linebreak
	 ret 
	 Sabado:
	 PutStr S1
	 PutStr A1
	 PutStr B1
	 PutStr A1
	 PutStr D1
	 PutStr O1
	 PutStr linebreak
	 PutStr S2
	 PutStr A2
	 PutStr B2
	 PutStr A2
	 PutStr D2
	 PutStr O2
	 PutStr linebreak
	 PutStr S1
	 PutStr A3
	 PutStr B3
	 PutStr A3
	 PutStr D2
	 PutStr O2
	 PutStr linebreak
	 PutStr S4
	 PutStr A2
	 PutStr B2
	 PutStr A2
	 PutStr D2
	 PutStr O2
	 PutStr linebreak
	 PutStr S1
	 PutStr A2
	 PutStr B1
	 PutStr A2
	 PutStr D1
	 PutStr O1
	 PutStr linebreak
	 ret
	 Domingo:
	 PutStr D1
	 PutStr O1
	 PutStr M1
	 PutStr I1
	 PutStr N1
	 PutStr G1
	 PutStr O1
	 PutStr linebreak
	 PutStr D2
	 PutStr O2
	 PutStr M1
	 PutStr I2
	 PutStr N1
	 PutStr G2
	 PutStr O2
	 PutStr linebreak
	 PutStr D2
	 PutStr O2
	 PutStr M3
	 PutStr I2
	 PutStr N3
	 PutStr G3
	 PutStr O2
	 PutStr linebreak
	 PutStr D2
	 PutStr O2
	 PutStr M1
	 PutStr I2
	 PutStr N4
	 PutStr G4
	 PutStr O2
	 PutStr linebreak
	 PutStr D1
	 PutStr O1
	 PutStr M1
	 PutStr I1
	 PutStr N1
	 PutStr G1
	 PutStr O1
	 PutStr linebreak

	 Monday:
	 
	 PutStr M1
	 PutStr O1
	 PutStr N1
	 PutStr D1
	 PutStr A1
	 PutStr Y1
	 PutStr linebreak
	 PutStr M1
	 PutStr O2
	 PutStr N1
	 PutStr D2
	 PutStr A2
	 PutStr Y2
	 PutStr linebreak
	 PutStr M3
	 PutStr O2
	 PutStr N3
	 PutStr D2
	 PutStr A3
	 PutStr Y3
	 PutStr linebreak
	 PutStr M1
	 PutStr O2
	 PutStr N4
	 PutStr D2
	 PutStr A2
	 PutStr Y3
	 PutStr linebreak
	 PutStr M1
	 PutStr O1
	 PutStr N1
	 PutStr D1
	 PutStr A2
	 PutStr Y3
	 PutStr linebreak
	 ret
	 Tuesday:
	 PutStr T1
	 PutStr U1
	 PutStr E1
	 PutStr S1
	 PutStr D1
	 PutStr A1
	 PutStr Y1
	 PutStr linebreak
	 PutStr T2
	 PutStr U1
	 PutStr E2
	 PutStr S2
	 PutStr D2
	 PutStr A2
	 PutStr Y2
	 PutStr linebreak
	 PutStr T2
	 PutStr U1
	 PutStr E1
	 PutStr S1
	 PutStr D2
	 PutStr A3
	 PutStr Y3
	 PutStr linebreak
	 PutStr T2
	 PutStr U1
	 PutStr E2
	 PutStr S4
	 PutStr D2
	 PutStr A2
	 PutStr Y3
	 PutStr linebreak
	 PutStr T2
	 PutStr U5
	 PutStr E1
	 PutStr S1
	 PutStr D1
	 PutStr A2
	 PutStr Y3
	 PutStr linebreak
	 ret
	 Wednesday:
	 PutStr W1
	 PutStr E1
	 PutStr D1
	 PutStr N1
	 PutStr E1
	 PutStr S1
	 PutStr D1
	 PutStr A1
	 PutStr Y1
	 PutStr linebreak
	 PutStr W1
	 PutStr E2
	 PutStr D2
	 PutStr N1
	 PutStr E2
	 PutStr S2
	 PutStr D2
	 PutStr A2
	 PutStr Y2
	 PutStr linebreak
	 PutStr W3
	 PutStr E1
	 PutStr D2
	 PutStr N3
	 PutStr E1
	 PutStr S1
	 PutStr D2
	 PutStr A3
	 PutStr Y3
	 PutStr linebreak
	 PutStr W4
	 PutStr E2
	 PutStr D2
	 PutStr N4
	 PutStr E2
	 PutStr S4
	 PutStr D2
	 PutStr A2
	 PutStr Y3
	 PutStr linebreak
	 PutStr W1
	 PutStr E1
	 PutStr D1
	 PutStr N1
	 PutStr E1
	 PutStr S1
	 PutStr D1
	 PutStr A2
	 PutStr Y3
	 PutStr linebreak
	 ret
	 Thursday:
	 
	 PutStr T1
	 PutStr H1
	 PutStr U1
	 PutStr R1
	 PutStr S1
	 PutStr D1
	 PutStr A1
	 PutStr Y1
	 PutStr linebreak
	 PutStr T2
	 PutStr H1
	 PutStr U1
	 PutStr R2
	 PutStr S2
	 PutStr D2
	 PutStr A2
	 PutStr Y2
	 PutStr linebreak
	 PutStr T2
	 PutStr H3
	 PutStr U1
	 PutStr R1
	 PutStr S1
	 PutStr D2
	 PutStr A3
	 PutStr Y3
	 PutStr linebreak
	 PutStr T2
	 PutStr H1
	 PutStr U1
	 PutStr R4
	 PutStr S4
	 PutStr D2
	 PutStr A2
	 PutStr Y3
	 PutStr linebreak
	 PutStr T2
	 PutStr H1
	 PutStr U5
	 PutStr R5
	 PutStr S1
	 PutStr D1
	 PutStr A2
	 PutStr Y3
	 PutStr linebreak
	 ret
	 Friday:
	 PutStr F1
	 PutStr R1
	 PutStr I1
	 PutStr D1
	 PutStr A1
	 PutStr Y1
	 PutStr linebreak
	 PutStr F2
	 PutStr R2
	 PutStr I2
	 PutStr D2
	 PutStr A2
	 PutStr Y2
	 PutStr linebreak
	 PutStr F3
	 PutStr R1
	 PutStr I2
	 PutStr D2
	 PutStr A3
	 PutStr Y3
	 PutStr linebreak
	 PutStr F2
	 PutStr R4
	 PutStr I2
	 PutStr D2
	 PutStr A2
	 PutStr Y3
	 PutStr linebreak
	 PutStr F2
	 PutStr R5
	 PutStr I1
	 PutStr D1
	 PutStr A2
	 PutStr Y3
	 PutStr linebreak
	 ret
	 Saturday:
	 PutStr S1
	 PutStr A1
	 PutStr T1
	 PutStr U1
	 PutStr R1
	 PutStr D1
	 PutStr A1
	 PutStr Y1
	 PutStr linebreak
	 PutStr S2
	 PutStr A2
	 PutStr T2
	 PutStr U1
	 PutStr R2
	 PutStr D2
	 PutStr A2
	 PutStr Y2
	 PutStr linebreak
	 PutStr S1
	 PutStr A3
	 PutStr T2
	 PutStr U1
	 PutStr R1
	 PutStr D2
	 PutStr A3
	 PutStr Y3
	 PutStr linebreak
	 PutStr S4
	 PutStr A2
	 PutStr T2
	 PutStr U1
	 PutStr R4
	 PutStr D2
	 PutStr A2
	 PutStr Y3
	 PutStr linebreak
	 PutStr S1
	 PutStr A2
	 PutStr T2
	 PutStr U5
	 PutStr R5
	 PutStr D1
	 PutStr A2
	 PutStr Y3
	 PutStr linebreak
	 ret
	 Sunday:
	 PutStr S1
	 PutStr U1
	 PutStr N1
	 PutStr D1
	 PutStr A1
	 PutStr Y1
	 PutStr linebreak
	 PutStr S2
	 PutStr U1
	 PutStr N1
	 PutStr D2
	 PutStr A2
	 PutStr Y2
	 PutStr linebreak
	 PutStr S1
	 PutStr U1
	 PutStr N3
	 PutStr D2
	 PutStr A3
	 PutStr Y3
	 PutStr linebreak
	 PutStr S4
	 PutStr U1
	 PutStr N4
	 PutStr D2
	 PutStr A2
	 PutStr Y3
	 PutStr linebreak
	 PutStr S1
	 PutStr U5
	 PutStr N1
	 PutStr D1
	 PutStr A2
	 PutStr Y3
	 PutStr linebreak
	 ret
  	 end:

     .EXIT      
